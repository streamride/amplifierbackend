#!/bin/bash

# Collect static files
echo "Collect static files"
python manage.py collectstatic --noinput

python manage.py makemigrations
echo "Apply database migrations"
python manage.py migrate