from django.contrib import admin
from reversion.admin import VersionAdmin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import Contact, SmokeBrand, ContactPhoto, AmplifierRound, ContactHistory, ContactLog, Round, \
    ContactGeo, FavoriteContact


class SmokeBrandResource(resources.ModelResource):
    class Meta:
        model = SmokeBrand
        fields = ('name', 'format', 'segment')
        import_id_fields = ('name', 'format',)


class SmokeBrandAdmin(ImportExportModelAdmin):
    resource_class = SmokeBrandResource


admin.site.register(SmokeBrand, SmokeBrandAdmin)
admin.site.register(ContactPhoto)
admin.site.register(AmplifierRound)
admin.site.register(ContactHistory)
admin.site.register(Round)
admin.site.register(ContactGeo)
admin.site.register(FavoriteContact)


# admin.site.register(ContactLog)


@admin.register(Contact)
class YourModelAdmin(VersionAdmin):
    pass
