import logging

from django.core.management import BaseCommand

from contacts.models import Contact

from api.v1.myqube_api import my_cube_check_email_status

logger = logging.getLogger('django')


class Command(BaseCommand):
    def handle(self, *args, **options):
        logger.debug('check email')
        contacts = Contact.objects.all()
        for contact in contacts:
            logger.debug(contact)
            my_cube_check_email_status(contact)
