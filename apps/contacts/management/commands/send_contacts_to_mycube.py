import logging

from django.core.management import BaseCommand

from contacts.models import Contact

from api.v1.myqube_api import add_to_mycube

logger = logging.getLogger('django')


class Command(BaseCommand):
    def handle(self, *args, **options):
        contacts = Contact.objects.filter(my_cube_contact_id__isnull=True)
        logger.debug(contacts)
        for contact in contacts:
            add_to_mycube(contact)
