import logging

from django.core.management import BaseCommand

from contacts.models import Contact
import datetime

logger = logging.getLogger('django')


class Command(BaseCommand):
    def handle(self, *args, **options):
        logger.debug('removing')
        Contact.objects.filter(registered_date__lt=datetime.date(2016, 11, 7)).delete()
