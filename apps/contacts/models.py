# coding: utf-8
import logging
import uuid as uuid

from django.conf import settings
from django.db import models
from datetime import datetime
from datetime import timedelta
import time
import uuid as uuid

from django.shortcuts import get_object_or_404
from events.models import Event
from utils.models import UCDateTimeField
from utils.views import to_timestamp

from utils.models import UploadToSpecDir

from amplifier_control.models import AmplifierGroup

logger = logging.getLogger('django')

contact_upload_dir = UploadToSpecDir('contacts')

base_url = settings.HOST_URL

end_of_day_time = 86399


class ContactManager(models.Manager):
    def is_info(self):
        return Contact.objects.filter(email__isnull=True)

    def is_reg(self):
        return Contact.objects.filter(email__isnull=False)

    def count_by_status(self, status, amplifier=None, start_date=None, end_date=None, aid=None):
        result = Contact.objects.filter(sent_status=status)
        if amplifier:
            result = result.filter(amplifier=amplifier)
        elif aid:
            amplifier_group = AmplifierGroup.objects.get_my_amplifiers(aid=aid)
            result = result.filter(amplifier__in=amplifier_group.values('pk'))
        else:
            result = result.filter(amplifier__user_type=0)
        if start_date and end_date:
            start_date = datetime.fromtimestamp(float(start_date))
            end_date = datetime.fromtimestamp(float(end_date)) + timedelta(days=1)
            result = result.filter(created__range=[start_date, end_date])
        return result.count()

    def get_by_user(self, user):
        if user.is_admin:
            return Contact.objects.all()
        else:
            return Contact.objects.filter(amplifier=user)

    def get_count(self, amplifier=None, start_date=None, end_date=None, aid=None):
        result = Contact.objects.all()
        if amplifier:
            result = result.filter(amplifier=amplifier)
        elif aid:
            amplifier_group = AmplifierGroup.objects.get_my_amplifiers(aid=aid)
            result = result.filter(amplifier__in=amplifier_group.values('pk'))
        else:
            result = result.filter(amplifier__user_type=0)
        if start_date and end_date:
            start_date = datetime.fromtimestamp(float(start_date))
            end_date = datetime.fromtimestamp(float(end_date)) + timedelta(days=1)
            result = result.filter(created__range=[start_date, end_date])
        return result.count()

    def get_all(self, start_date=None, end_date=None, aid=None):
        result = Contact.objects.all()
        if aid:
            amplifier_group = AmplifierGroup.objects.get_my_amplifiers(aid=aid)
            if amplifier_group:
                result = result.filter(amplifier__in=amplifier_group.values('pk'))
        else:
            result = result.filter(amplifier__user_type=0)
        if start_date and end_date:
            start_date = datetime.fromtimestamp(float(start_date))
            end_date = datetime.fromtimestamp(float(end_date)) + timedelta(days=1)
            result = result.filter(created__range=[start_date, end_date])
        return result


class Contact(models.Model):
    STATUS_NOT_SYNC = 0
    STATUS_NOT_DELIVERED = 1
    STATUS_NOT_OPENED = 2
    STATUS_OPENED = 3

    STATUSES = (
        (0, 'not_sync'),
        (1, 'mail_not_delivered'),
        (2, 'mail_not_opened'),
        (3, 'mail_opened'),
    )

    MAIL_STATUSES = (
        (0, 'not_delivered'),
        (1, 'delivered'),
        (2, 'spam'),
    )
    sent_status = models.IntegerField(max_length=100, null=True, blank=True, choices=MAIL_STATUSES, default=0)
    open_status = models.BooleanField(default=False)
    click_status = models.BooleanField(default=False)
    unsubscribe_status = models.BooleanField(default=False)
    status = models.IntegerField(max_length=100, null=True, blank=True, choices=STATUSES, default=0)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    created = models.DateTimeField('дата создания', auto_now_add=True)
    updated = models.DateTimeField('дата последнего изменения', auto_now=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    surname = models.CharField(max_length=200, null=True, blank=True)
    phone = models.CharField(max_length=50, null=True, blank=True)
    email = models.EmailField(blank=True, null=True)
    birthday = UCDateTimeField(blank=True, null=True)
    is_smoke = models.BooleanField(default=False)
    comment = models.TextField(null=True, blank=True)
    is_repeat = models.BooleanField(default=False)
    main_smoke_brand = models.ForeignKey('contacts.SmokeBrand', related_name='main_smoke_brand', null=True, blank=True)
    smoke_brands = models.ManyToManyField('contacts.SmokeBrand', related_name='smoke_brands', null=True, blank=True)
    event = models.ManyToManyField(Event, related_name='contact_event', null=True, blank=True)
    additional_events = models.ManyToManyField(Event, related_name='contact_add_events', null=True, blank=True)
    is_registered = models.BooleanField(default=False)
    amplifier = models.ForeignKey('users.User')
    is_agreed = models.BooleanField(default=False)
    fb_id = models.CharField(max_length=100, null=True, blank=True)
    vk_id = models.CharField(max_length=100, null=True, blank=True)
    instagram_id = models.CharField(max_length=100, null=True, blank=True)
    photos = models.ManyToManyField('contacts.ContactPhoto', related_name='contact_photos', null=True, blank=True)
    registered_date = models.DateTimeField(blank=True, null=True)
    is_email_valid = models.BooleanField(default=False)
    avatar = models.ImageField(upload_to=contact_upload_dir, blank=True, null=True)
    avatar_from_set = models.BooleanField(default=0)
    my_cube_contact_id = models.CharField(max_length=100, null=True, blank=True)
    objects = ContactManager()

    def __str__(self):
        return '{} {} - {} {}'.format(self.name, self.surname, self.my_cube_contact_id, self.created)

    @staticmethod
    def create_or_update_status(data):
        id = data.get('id')
        contact = Contact.objects.filter(my_cube_contact_id=id).first()
        if contact is not None:
            sent_status = data.get('sent_status')
            logger.debug(sent_status)
            if sent_status == 'delivered':
                contact.sent_status = 1
            elif sent_status == 'spam':
                contact.sent_status = 2
            else:
                contact.sent_status = 0
            contact.open_status = data.get('open_status', 0)
            contact.click_status = data.get('click_status', 0)
            contact.unsubscribe_status = data.get('unsubscribe_status', 0)
            if contact.click_status:
                contact.open_status = 1
            if contact.unsubscribe_status:
                contact.open_status = 1
                contact.click_status = 1
            contact.save()

    def get_status_sent_ru(self):
        if self.sent_status == 0:
            return u'Не доступен'
        elif self.sent_status == 1:
            return u'Доступен'
        else:
            return 'Адресат считает спамом'

    def is_delivered(self):
        if self.sent_status == 1 or self.sent_status == 2:
            return 'yes'
        else:
            return 'no'

    def is_email_exists(self):
        return self.email is not None

    @staticmethod
    def create(data, amplifier):
        contact = Contact()
        contact = Contact.parse_and_save(contact, data, amplifier)
        return contact

    @staticmethod
    def update(contact, data):
        return Contact.parse_and_save(contact, data, contact.amplifier)

    @staticmethod
    def parse_and_save(contact, data, amplifier):
        if 'name' in data:
            contact.name = data.get('name')
        if 'surname' in data:
            contact.surname = data.get('surname')
        if 'phone' in data:
            contact.phone = data.get('phone')
        if 'email' in data:
            contact.email = data.get('email')
        if 'birthday' in data:
            contact.birthday = datetime.fromtimestamp(float(data.get('birthday')))
        if 'is_smoke' in data:
            contact.is_smoke = data.get('is_smoke')
        if 'is_repeat' in data:
            contact.is_repeat = data.get('is_repeat')
        if 'is_registered' in data:
            contact.is_registered = data.get('is_registered')
        if 'main_smoke_brand' in data:
            smoke_brand = SmokeBrand.objects.filter(id=data.get('main_smoke_brand'))
            if len(smoke_brand) > 0:
                contact.main_smoke_brand = smoke_brand[0]
        if 'vk' in data:
            contact.vk_id = data.get('vk')
        if 'fb' in data:
            contact.fb_id = data.get('fb')
        if 'instagram' in data:
            contact.instagram_id = data.get('instagram')
        if 'registered_date' in data:
            contact.registered_date = datetime.fromtimestamp(float(data.get('registered_date')))
        contact.amplifier = amplifier
        if 'avatar_from_set' in data:
            contact.avatar_from_set = data.get('avatar_from_set')
        contact.save()

        if 'photos' in data:
            contact.photos.clear()
            if data['photos']:
                photos = data.get('photos', '')
                split = photos.strip().split(',')
                if photos:
                    photo_list = ContactPhoto.objects.filter(uuid__in=split)
                    for photo in photo_list:
                        contact.photos.add(photo)
        if 'other_smoke_brands' in data:
            contact.smoke_brands.clear()
            if data['other_smoke_brands']:
                brands = data.get('other_smoke_brands', '')
                split = brands.strip().split(',')
                logger.debug(split)
                logger.debug(len(split))
                if brands:
                    for str in split:
                        smoke_brand = get_object_or_404(SmokeBrand, pk=str)
                        if contact.smoke_brands.filter(smoke_brands__id__exact=smoke_brand.id).count() == 0:
                            contact.smoke_brands.add(smoke_brand)
        if 'events' in data:
            contact.event.clear()
            if data['events']:
                event_split = data.get('events').split(',')
                for str in event_split:
                    event = get_object_or_404(Event, uuid=str)
                    if contact.event.filter(contact_event__id__exact=event.id).count() == 0:
                        contact.event.add(event)
        if 'additional_events' in data:
            contact.additional_events.clear()
            if data['additional_events']:
                event_split = data.get('additional_events').split(',')
                for str in event_split:
                    event = get_object_or_404(Event, uuid=str)
                    if contact.additional_events.filter(contact_event__id__exact=event.id).count() == 0:
                        contact.additional_events.add(event)

        return contact

    def get_avatar_url(self):
        if not self.avatar:
            return ''
        return '{}{}'.format(base_url, self.avatar.url)

    def get_status(self):
        if self.is_email_exists():
            if self.sent_status == 0:
                return Contact.STATUS_NOT_DELIVERED
            else:
                if self.open_status == 1:
                    return Contact.STATUS_OPENED
                else:
                    return Contact.STATUS_NOT_OPENED
        else:
            return Contact.STATUS_NOT_SYNC

    def as_json(self):
        return dict(
            id=self.uuid,
            name=self.name,
            surname=self.surname,
            phone=self.phone,
            email=self.email,
            is_smoke=self.is_smoke,
            is_repeat=self.is_repeat,
            is_registered=self.is_registered,
            birthday=to_timestamp(self.birthday) if self.birthday is not None else '',
            fb=self.fb_id or '',
            vk=self.vk_id or '',
            instagram=self.instagram_id or '',
            is_agreed=self.is_agreed,
            smoke_brand=self.main_smoke_brand.name if self.main_smoke_brand is not None else '',
            other_smoke_brands=[smoke_brand.as_json() for smoke_brand in self.smoke_brands.all()],
            status=self.get_status(),
            registered_date=to_timestamp(self.registered_date) if self.registered_date is not None else '',
            contact_photos=[photo.as_json() for photo in self.photos.all()],
            events=[event.as_json(False) for event in self.event.all()],
            additional_events=[event.as_json(False) for event in self.additional_events.all()],
            avatar=self.get_avatar_url(),
            type=self.get_contact_type()
        )

    def get_vk(self):
        if self.vk_id:
            return 'https://vk.com/' + self.vk_id
        return ''

    def get_fb(self):
        if self.fb_id:
            return 'https://facebook.com/' + self.fb_id
        return ''

    def get_instagram(self):
        if self.instagram_id:
            return 'https://instagram.com/' + self.instagram_id
        return ''

    def get_contact_type(self):
        return ContactHistory.objects.get_last_contact_type(self)

    def get_brand1_name(self):
        if self.main_smoke_brand is not None:
            return self.main_smoke_brand.name
        return ''

    def get_brand2_name(self):
        if self.smoke_brands is not None and self.smoke_brands.count() > 0:
            return self.smoke_brands.all()[0].name
        return ''

    def get_event_name(self):
        if self.event is not None and self.event.count() > 0:
            return self.event.all()[0].name
        return ''

    def get_birthday_timestamp(self):
        return to_timestamp(self.birthday) if self.birthday is not None else ''

    def get_comment(self):
        if self.comment is not None:
            return str(self.comment)
        return ''

    def get_phone(self):
        if self.phone:
            return self.phone
        return ''

    def get_email(self):
        if self.email:
            return self.email
        return ''

    def get_mycube(self):
        return self.amplifier.mycube_id if self.amplifier.mycube_id else ''

    def get_geo(self):
        return ContactGeo.objects.get_yandex_link(self)

    def get_info_date(self):
        contact_history = ContactHistory.objects.filter(contact=self).filter(type=0).first()
        if contact_history is not None:
            return contact_history.updated.strftime('%d.%m.%Y')
        return 'no'

    def get_reg_date(self):
        contact_history = ContactHistory.objects.filter(contact=self).filter(type=1).first()
        if contact_history is not None:
            return contact_history.updated.strftime('%d.%m.%Y')
        return 'no'

    def get_repeat_date(self):
        contact_history = ContactHistory.objects.filter(contact=self).filter(type=2).first()
        if contact_history is not None:
            return contact_history.updated.strftime('%d.%m.%Y')
        return 'no'

    def get_photo_confirm(self):
        photo = self.photos.first()
        if photo is not None:
            return photo.get_full_url()
        return 'no'


# not used
class ContactLog(Contact):
    updated_log = models.DateTimeField('дата последнего изменения', auto_now=True)

    def __str__(self):
        return '{} {}'.format(self.name, self.surname)


class ContactHistoryManager(models.Manager):
    def get_info_count(self, amplifier=None, start_date=None, end_date=None, aid=None):
        return self.get_count_by_type(0, amplifier, end_date, start_date, aid)

    def get_reg_count(self, amplifier=None, start_date=None, end_date=None, aid=None):
        return self.get_count_by_type(1, amplifier, end_date, start_date, aid)

    def get_repeat_count(self, amplifier=None, start_date=None, end_date=None, aid=None):
        return self.get_count_by_type(2, amplifier, end_date, start_date, aid)

    def get_count_by_type(self, type, amplifier, end_date, start_date, aid):
        result = ContactHistory.objects.filter(type=type)
        if amplifier is not None:
            result = result.filter(amplifier=amplifier)
        elif aid:
            amplifier_group = AmplifierGroup.objects.get_my_amplifiers(aid=aid)
            result = result.filter(amplifier__in=amplifier_group.values('pk'))
        else:
            result = result.filter(amplifier__user_type=0)
        if start_date and end_date:
            start_date = datetime.fromtimestamp(float(start_date))
            end_date = datetime.fromtimestamp(float(end_date)) + timedelta(days=1)
            result = result.filter(contact__created__range=[start_date, end_date])
        return result.count()

    def get_info_current_round_count(self, amplifier, start_date, end_date):
        return ContactHistory.objects.filter(amplifier=amplifier).filter(type=0).filter(updated__gt=start_date).filter(
            updated__lt=end_date).count()

    def get_reg_current_round_count(self, amplifier, start_date, end_date):
        return ContactHistory.objects.filter(amplifier=amplifier).filter(type=1).filter(updated__gt=start_date).filter(
            updated__lt=end_date).count()

    def get_repeat_current_round_count(self, amplifier, start_date, end_date):
        return ContactHistory.objects.filter(amplifier=amplifier).filter(type=2).filter(updated__gt=start_date).filter(
            updated__lt=end_date).count()

    def get_reg_opened_count(self, amplifier):
        contact_history_list = ContactHistory.objects.filter(amplifier=amplifier).filter(type=1).values('contact')
        return Contact.objects.filter(id__in=contact_history_list).filter(open_status=True).count()

    def get_reg_not_opened_count(self, amplifier):
        contact_history_list = ContactHistory.objects.filter(amplifier=amplifier).filter(type=1).values('contact')
        return Contact.objects.filter(id__in=contact_history_list).filter(sent_status=1).filter(
            open_status=False).count()

    def get_reg_not_delivered_count(self, amplifier):
        contact_history_list = ContactHistory.objects.filter(amplifier=amplifier).filter(type=1).values('contact')
        return Contact.objects.filter(id__in=contact_history_list).filter(sent_status=0).count()

    def get_last_contact_type(self, contact):

        # max_updated = ContactHistory.objects.filter(contact=contact).latest('updated').updated
        contact_history_list = ContactHistory.objects.filter(contact=contact).order_by('-updated')
        contact = None
        if contact_history_list.count() > 0:
            contact = contact_history_list[0]
        # logger.debug(contact_history_list)
        # contact = ContactHistory.objects.filter(contact=contact).latest('updated')
        # contact = contact_history_list.first()
        if contact is not None:
            if contact.type == 0:
                return 'info'
            elif contact.type == 1:
                return 'register'
            else:
                return 'repeat'
        return ''


class ContactHistory(models.Model):
    STATUS_INFO = 0
    STATUS_REG = 1
    STATUS_REPEAT = 2
    TYPES = (
        (0, 'INFO'),
        (1, 'REG'),
        (2, 'REPEAT'),
    )
    contact = models.ForeignKey(Contact)
    amplifier = models.ForeignKey('users.User')
    type = models.IntegerField(max_length=100, null=True, blank=True, choices=TYPES, default=0)
    updated = models.DateTimeField('дата последнего изменения', auto_now=True)
    objects = ContactHistoryManager()

    def __str__(self):
        return '{}{}{}'.format(self.contact.name, self.amplifier.email, self.type)

    @staticmethod
    def set_status(contact, status, user):
        contact_history, created = ContactHistory.objects.get_or_create(contact=contact, amplifier=user, type=status)

    @staticmethod
    def remove_other_statuses(contact):
        ContactHistory.remove_by_status(contact, ContactHistory.STATUS_REG)
        ContactHistory.remove_by_status(contact, ContactHistory.STATUS_INFO)

    @staticmethod
    def remove_by_status(contact, status):
        try:
            contact_history = ContactHistory.objects.get(contact=contact, type=status,
                                                         amplifier=contact.amplifier)
            if contact_history:
                contact_history.delete()
        except:
            pass


# class ContactStatus(models.Model):
#     STATUSES = (
#         (0, 'not_delivered'),
#         (1, 'delivered'),
#         (2, 'spam'),
#     )
#     contact = models.ForeignKey('contacts.Contact', related_name='contact')
#     sent_status = models.IntegerField(max_length=100, null=True, blank=True, choices=STATUSES, default=0)
#     open_status = models.BooleanField(default=False)
#     click_status = models.BooleanField(default=False)
#     unsubscribe_status = models.BooleanField(default=False)
#     created = models.DateTimeField('дата создания', auto_now_add=True)
#     updated = models.DateTimeField('дата последнего изменения', auto_now=True)
#
#     def __str__(self):
#         return '{}'.format(self.contact.name)
#
#     @staticmethod
#     def create_or_update(data):
#         id = data.get('id')
#         contact = Contact.objects.filter(my_cube_contact_id=id).first()
#         contact_status = ContactStatus.objects.filter(contact=contact).first()
#         if contact_status is None:
#             contact_status = ContactStatus()
#         if contact is not None:
#             sent_status = data.get('sent_status')
#             if sent_status is 'delivered':
#                 contact_status.sent_status = 1
#             elif sent_status is 'spam':
#                 contact_status.sent_status = 3
#             else:
#                 contact_status.sent_status = 0
#             contact_status.open_status = data.get('open_status', 0)
#             contact_status.click_status = data.get('click_status', 0)
#             contact_status.unsubscribe_status = data.get('unsubscribe_status', 0)
#             contact_status.contact = contact
#             contact_status.save()
#
#     def get_status_sent_ru(self):
#         if self.sent_status == 0:
#             return u'Не доступен'
#         elif self.sent_status == 1:
#             return u'Доступен'
#         else:
#             return 'Адресат считает спамом'
#
#     def is_delivered(self):
#         if self.sent_status == 1 or self.sent_status == 2:
#             return 'yes'
#         else:
#             return 'no'


class SmokeBrand(models.Model):
    # uuid = models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    format = models.CharField(max_length=50, blank=True, null=True)
    segment = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return '{} {}'.format(self.format, self.name)

    def as_json(self):
        return dict(
            id=self.pk,
            name=self.name,
            format=self.format,
            segment=self.segment
        )


class ContactPhoto(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    photo = models.ImageField(upload_to=contact_upload_dir, blank=True, null=True)

    def __str__(self):
        return u'{}'.format(self.photo.url)

    def as_json(self):
        return dict(
            id=self.uuid,
            url=self.get_full_url()
        )

    def get_full_url(self):
        return '{}{}'.format(base_url, self.photo.url)


class AmplifierRoundManager(models.Manager):
    def get_current_round(self, amplifier):
        rounds = Round.objects.filter(start_date__lt=datetime.now()).filter(
            end_date__gt=datetime.now())
        if len(rounds):
            return rounds[0]
        else:
            return None


class AmplifierRound(models.Model):
    amplifier = models.ForeignKey('users.User')
    round = models.ForeignKey('contacts.Round', related_name='round', null=True, blank=True)
    objects = AmplifierRoundManager()

    def __str__(self):
        return '{}'.format(self.amplifier.username)


class Round(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    start_date = models.DateTimeField('дата начала', null=True, blank=True)
    end_date = models.DateTimeField('Дата окончания', null=True, blank=True)
    kpi_reg_count = models.IntegerField(default=0)
    kpi_info_count = models.IntegerField(default=0)
    kpi_repeat_count = models.IntegerField(default=0)

    def __str__(self):
        return '{} {} {}'.format(self.name, self.start_date, self.end_date)

    def as_json(self):
        return dict(
            id=self.pk,
            name=self.name,
            start_date=to_timestamp(self.start_date) if self.start_date is not None else '',
            end_date=to_timestamp(self.end_date) if self.end_date is not None else '',
            kpi_reg=self.kpi_reg_count,
            kpi_info=self.kpi_info_count,
            kpi_repeat=self.kpi_repeat_count
        )


class ContactGeoManager(models.Manager):
    def get_yandex_link(self, contact):
        contact_geo = ContactGeo.objects.filter(contact=contact).first()
        if contact_geo:
            return 'http://maps.yandex.ru/?ll={},{}&pt={},{}'.format(contact_geo.longitude, contact_geo.latitude,
                                                                     contact_geo.longitude, contact_geo.latitude)


class ContactGeo(models.Model):
    longitude = models.DecimalField(max_digits=19, decimal_places=10, blank=True)
    latitude = models.DecimalField(max_digits=19, decimal_places=10, blank=True)
    contact = models.ForeignKey('contacts.Contact', related_name='geo_contact')
    created = models.DateTimeField('дата создания', auto_now_add=True)
    updated = models.DateTimeField('дата последнего изменения', auto_now=True)
    objects = ContactGeoManager()

    def __str__(self):
        return '{} - lng: {}, lat: {}'.format(self.contact.name, self.longitude, self.latitude)


class FavoriteContactManager(models.Manager):
    def get_my_favorites(self, amplifier):
        return FavoriteContact.objects.filter(amplifier=amplifier)


class FavoriteContact(models.Model):
    contact = models.ForeignKey('contacts.Contact', related_name='favorite_contact')
    amplifier = models.ForeignKey('users.User')
    created = models.DateTimeField('дата создания', auto_now_add=True)
    updated = models.DateTimeField('дата последнего изменения', auto_now=True)
    objects = FavoriteContactManager()

    def __str__(self):
        return '{} {}'.format(self.contact, self.amplifier.email)

    def as_json(self):
        return dict(
            id=self.pk,
            contact=self.contact.as_json()
        )
