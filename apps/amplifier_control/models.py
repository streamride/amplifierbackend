from django.db import models


# Create your models here.


class AmplifierGroupManager(models.Manager):
    def get_my_amplifiers(self, aid):
        amplifier_group = AmplifierGroup.objects.filter(amplifier_admin__pk=aid).first()
        from users.models import User
        if amplifier_group:
            return User.objects.filter(pk__in=amplifier_group.amplifiers.values('pk'))
        return User.objects.filter(user_type=0)


class AmplifierGroup(models.Model):
    amplifiers = models.ManyToManyField('users.User', related_name='amplifiers_list', null=True, blank=True)
    amplifier_admin = models.ForeignKey('users.User')
    objects = AmplifierGroupManager()

    def __str__(self):
        return '{} {}'.format(self.amplifier_admin.first_name, self.amplifier_admin.last_name)

    class Meta:
        verbose_name = u'Группа amplifier'
        verbose_name_plural = u'Группы amplifier'
