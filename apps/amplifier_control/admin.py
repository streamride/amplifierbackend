from django.contrib import admin

# Register your models here.
from django import forms
from users.models import User

# Register your models here.
from amplifier_control.models import AmplifierGroup


class AmplifierGroupAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AmplifierGroupAdminForm, self).__init__(*args, **kwargs)
        # access object through self.instance...
        self.fields['amplifiers'].queryset = User.objects.filter(user_type=0)
        self.fields['amplifier_admin'].queryset = User.objects.filter(user_type=1)


class AmplifierGroupAdmin(admin.ModelAdmin):
    form = AmplifierGroupAdminForm


admin.site.register(AmplifierGroup, AmplifierGroupAdmin)
