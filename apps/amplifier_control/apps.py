from django.apps import AppConfig


class AmplifierControlConfig(AppConfig):
    name = 'amplifier_control'
