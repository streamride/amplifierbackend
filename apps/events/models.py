import logging
import os

from django.conf import settings
from django.db import models
from datetime import datetime
import time
from django.core.files.storage import default_storage
from django.utils.deconstruct import deconstructible
import uuid as uuid
from utils.views import to_timestamp
from utils.models import UCDateTimeField

from utils.models import UploadToSpecDir

events_upload_dir = UploadToSpecDir('events')


class Event(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    created = models.DateTimeField('дата создания', auto_now_add=True)
    updated = models.DateTimeField('дата последнего изменения', auto_now=True)
    name = models.CharField(max_length=400, null=True, blank=True)
    city = models.CharField(max_length=300, null=True, blank=True)
    address = models.TextField(blank=True, null=True)
    event_date = UCDateTimeField(null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    link = models.CharField(max_length=100, null=True, blank=True)
    amplifier = models.ForeignKey('users.User', null=True, blank=True)
    event_photos = models.ManyToManyField('events.EventPhoto', related_name='photos', null=True, blank=True)

    def __str__(self):
        return '{} {}'.format(self.name, self.amplifier)

    def as_json(self, with_amplifier=True):
        return dict(
            id=self.uuid,
            created_date=to_timestamp(self.created),
            updated_date=to_timestamp(self.updated),
            link=self.link,
            name=self.name,
            city=self.city,
            address=self.address,
            comment=self.comment,
            date=to_timestamp(self.event_date),
            amplifier=self.amplifier.as_amplifier() if with_amplifier else self.amplifier.pk,
            event_photos=[photo.as_json() for photo in self.event_photos.all()]
        )

    @staticmethod
    def create(data, amplifier):
        event = Event()
        event = Event.parse_and_save(data, event, amplifier)
        event.amplifier = amplifier
        return event

    @staticmethod
    def update(event, data):
        event = Event.parse_and_save(data, event, event.amplifier)
        return event

    @staticmethod
    def parse_and_save(data, event, amplifier):
        if 'name' in data:
            event.name = data.get('name', '')
        if 'city' in data:
            event.city = data.get('city', '')
        else:
            event.city = amplifier.city
        if 'address' in data:
            event.address = data.get('address', '')
        if 'date' in data:
            event.event_date = datetime.fromtimestamp(float(data.get('date', '')))
        if 'comment' in data:
            event.comment = data.get('comment', '')
        if 'photos' in data:
            event.save()
            event.event_photos.clear()
            photos = data.get('photos', '')
            split = photos.strip().split(',')
            if photos:
                photo_list = EventPhoto.objects.filter(uuid__in=split)
                for photo in photo_list:
                    event.event_photos.add(photo)
        if 'link' in data:
            event.link = data.get('link')
        return event


class EventPhoto(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    photo = models.ImageField(upload_to=events_upload_dir, blank=True, null=True)

    def __str__(self):
        return '{}'.format(self.photo.url)

    def as_json(self):
        return dict(
            id=self.uuid,
            url=self.get_full_url()
        )

    def get_full_url(self):
        return '{}{}'.format(settings.HOST_URL, self.photo.url)
