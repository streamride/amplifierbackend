from django.contrib import admin

from .models import Event, EventPhoto
from reversion.admin import VersionAdmin

# admin.site.register(Event)
admin.site.register(EventPhoto)


@admin.register(Event)
class YourModelAdmin(VersionAdmin):
    pass
