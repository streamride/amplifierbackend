__author__ = 'andreyzakharov'

# SWAGGER_SETTINGS = {
#     'exclude_namespaces': [],
#     'api_version': '0.1',
#     'api_path': '/',
#     'enabled_methods': [
#         'get',
#         'post',
#         'put',
#         'patch',
#         'delete'
#     ],
#     'api_key': '',
#     'is_authenticated': False,
#     'is_superuser': False,
#     'permission_denied_handler': None
# }

REST_FRAMEWORK = {
    # 'DEFAULT_PARSER_CLASSES': (
    #     'rest_framework.parsers.JSONParser',
    # ),

    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',

    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),

    # 'DEFAULT_VERSIONING_CLASS': 'rest_framework.versioning.NamespaceVersioning'
}
