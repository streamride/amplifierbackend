# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.views import password_reset_confirm
from django.contrib.auth.views import password_reset
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect

# Create your views here.
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect
from django.views.generic import FormView, View
from rest_framework.authtoken.models import Token
from users.forms import RegistrationForm, AuthForm
from django.contrib.auth import login as auth_login, logout as auth_logout


class RegistrationFormView(FormView):
    form_class = RegistrationForm

    def form_valid(self, form):
        user = form.save()
        token, created = Token.objects.get_or_create(user=user)
        return redirect('home')

    def get_context_data(self, **kwargs):
        data = super(FormView, self).get_context_data(**kwargs)
        data['title'] = u'Регистрация'
        return data


class AuthFormView(FormView):
    form_class = AuthForm
    redirect_field_name = REDIRECT_FIELD_NAME
    success_url = 'current_profile_user'

    def form_valid(self, form):
        self.user = form.get_user()
        auth_login(self.request, self.user)
        return super(AuthFormView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        data = super(AuthFormView, self).get_context_data(**kwargs)
        data['title'] = u'Авторизация'
        return data

    def get_success_url(self):
        if self.request.REQUEST.get(self.redirect_field_name, ''):
            return self.request.REQUEST.get(self.redirect_field_name, '')
        else:
            return '/users/profile/'

    @method_decorator(csrf_protect)
    def dispatch(self, *args, **kwargs):
        return super(AuthFormView, self).dispatch(*args, **kwargs)


class LogoutView(View):
    def get(self, request, *args, **kwargs):
        auth_logout(request)
        return HttpResponseRedirect(settings.LOGOUT_REDIRECT)


def reset_user_password(request):
    return password_reset(request, template_name='users/forgot_password.html',
                          email_template_name='users/reset_email.html',
                          subject_template_name='users/reset_subject.txt',
                          post_reset_redirect=reverse('success_sent'))


def reset_confirm(request, uidb64=None, token=None):
    # Wrap the built-in reset confirmation view and pass to it all the captured parameters like uidb64, token
    # and template name, url to redirect after password reset is confirmed.
    return password_reset_confirm(request, template_name='users/forgot_password.html',
                                  uidb64=uidb64, token=token, post_reset_redirect=reverse('success_changed'))


def success_changed(request):
    return render(request, "users/")


def success(request):
    return render(request, "users/success.html")


def success_changed(request):
    return render(request, "users/success_changed.html")
