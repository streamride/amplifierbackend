from django.contrib.auth import login
from django.shortcuts import redirect
from rest_framework.authtoken.models import Token
from social.apps.django_app.utils import strategy, psa
from social.backends.oauth import BaseOAuth1, BaseOAuth2

from social.pipeline.partial import partial
from users.models import User
from django.http import JsonResponse, HttpResponseBadRequest

USER_FIELDS = ['username', 'email']


@partial
def require_email(strategy, details, user=None, is_new=False, *args, **kwargs):
    if user and user.email:
        return
    elif is_new and not details.get('email'):
        if strategy.session_get('saved_email'):
            details['email'] = strategy.session_pop('saved_email')
        else:
            return ''


# @dsa_view()
@psa('social:complete')
def register_by_access_token(request, backend):
    if isinstance(request.backend, BaseOAuth1):
        access_token = {
            'oauth_token': request.REQUEST.get('access_token'),
            'oauth_token_secret': request.REQUEST.get('access_token_secret'),
        }
    elif isinstance(request.backend, BaseOAuth2):
        access_token = request.REQUEST.get('access_token')
    else:
        raise HttpResponseBadRequest('Wrong backend type')

    # access_token = request.GET.get('access_token', None)
    # social = request.user.social_auth.get(provider=backend_p)

    # token = request.GET.get('access_token')
    backend = request.backend
    user = backend.do_auth(access_token)
    if user:
        login(request, user)
        token, created = Token.objects.get_or_create(user=user)

        return JsonResponse(user.current(token.key))
    else:
        return JsonResponse("Not authorized")


# def create_user(strategy, details, response, uid, user=None, *args, **kwargs):
#     if user:
#         return
#
#     fields = dict((name, kwargs.get(name) or details.get(name))
#                   for name in strategy.setting('USER_FIELDS',
#                                                USER_FIELDS))
#
#     if not fields:
#         return
#
#     email = fields.pop('email', '')
#     username = fields.pop('username', '')
#
#     if (not email) and username:
#         email = ''.join([
#             username,
#             '@none-',
#             str(time.time()).split('.')[0],
#             str(randint(1000, 9999)),
#             '.none'
#         ])
#     print fields
#     user = strategy.create_user(username, email, **fields)
#     user.is_active = True
#     user.save()
#
#     return {
#         'is_new': True,
#         'user': user
#     }


def associate_by_email(**kwargs):
    try:
        email = kwargs['details']['email']
        kwargs['user'] = User.objects.get(email=email)
    except:
        pass
    return kwargs


def create_user(strategy, details, is_new=False, *args, **kwargs):
    fields = dict((name, kwargs.get(name) or details.get(name))
                  for name in strategy.setting('USER_FIELDS',
                                               USER_FIELDS))
    if is_new:
        email = fields.pop('email', '')
        username = fields.pop('username', '')
        try:
            user = User.objects.get(username=username)
        except:
            user = None

        if user is None:
            user = strategy.create_user(email, username, **fields)
        user.is_active = True
        user.save()
        return {
            'is_new': True,
            'user': user
        }
    else:
        return
