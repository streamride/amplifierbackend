# -*- coding: utf-8 -*-
from datetime import datetime

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils.translation import ugettext as _

from contacts.models import AmplifierRound

from utils.models import UploadToSpecDir

from contacts.models import ContactHistory

user_upload_dir = UploadToSpecDir('profile')


class UserManager(BaseUserManager):
    def create_user(self, email, username, first_name, last_name, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=UserManager.normalize_email(email),
            first_name=first_name,
            username=username,
            last_name=last_name,
            password=password
        )

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, first_name, last_name, password=None):
        user = self.create_user(
            email=email,
            username=email,
            password=password,
            first_name=first_name,
            last_name=last_name,
        )
        user.is_staff = True
        user.is_admin = True
        user.save(using=self._db)
        return user

    def get_user_by_email(self, email):
        user = self.get(email=email)
        if user:
            return user
        else:
            return None


class User(AbstractBaseUser, PermissionsMixin):
    USER_STATUSES = (
        (0, 'amplifier'),
        (1, 'amplifier_admin'),
        (2, 'admin'),
    )
    email = models.CharField(_("E-mail"), max_length=75, unique=True)
    username = models.CharField(_("Username"), max_length=100, null=True, blank=True)
    first_name = models.CharField(_("First name"), max_length=30, null=True, blank=True)
    last_name = models.CharField(_("Last name"), max_length=30, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    birthday = models.DateField(auto_now=False, null=True, blank=True)
    is_staff = models.BooleanField(default=False)
    objects = UserManager()
    date_joined = models.DateTimeField(default=datetime.now())
    avatar = models.ImageField(upload_to=user_upload_dir, blank=True, null=True)
    country = models.CharField(max_length=200, blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    info = models.TextField(blank=True, null=True)
    mycube_id = models.CharField(max_length=100, null=True, blank=True)
    user_type = models.IntegerField(max_length=100, null=True, blank=True, choices=USER_STATUSES, default=0)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def get_avatar_url(self):
        if self.avatar and self.avatar != "1":
            return u"/media/" + unicode(self.avatar)
        else:
            return u"http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&s=50"

    def get_full_name(self):
        return u'%s %s' % (self.first_name, self.last_name)

    def get_short_name(self):
        return u'%s' % self.first_name

    def __unicode__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    def get_user_group(self):
        return

    def get_absolute_url(self):
        return reverse('profile_user', kwargs={'pk': self.pk})

    @property
    def avatar_url(self):
        if self.avatar and hasattr(self.avatar, 'url'):
            return self.avatar.url
        else:
            return ''

    def current(self, token):
        return dict(
            id=self.id,
            first_name=self.first_name,
            last_name=self.last_name,
            email=self.email,
            username=self.username,
            token=token
        )

    def as_json(self):
        return dict(
            id=self.id,
            first_name=self.first_name,
            last_name=self.last_name,
            email=self.email,
            username=self.username,
        )

    def info_contacts_count(self):
        return ContactHistory.objects.get_info_count(self)

    def reg_contacts_count(self):
        return ContactHistory.objects.get_reg_count(self)

    def repeat_contact(self):
        return ContactHistory.objects.get_repeat_count(self)

    def current_repeat_contact(self):
        round = AmplifierRound.objects.get_current_round(self)
        if round is not None:
            return ContactHistory.objects.get_repeat_current_round_count(self, round.start_date, round.end_date)
        else:
            return self.repeat_contact()

    def current_info_contacts(self):
        round = AmplifierRound.objects.get_current_round(self)
        if round is not None:
            return ContactHistory.objects.get_info_current_round_count(self, round.start_date, round.end_date)
        else:
            return self.info_contacts_count()

    def current_reg_contacts(self):
        round = AmplifierRound.objects.get_current_round(self)
        if round is not None:
            return ContactHistory.objects.get_reg_current_round_count(self, round.start_date, round.end_date)
        else:
            return self.reg_contacts_count()

    def opened_contacts_count(self):
        return ContactHistory.objects.get_reg_opened_count(self)

    def not_opened_contacts_count(self):
        return ContactHistory.objects.get_reg_not_opened_count(self)

    def not_delivered_contacts_count(self):
        return ContactHistory.objects.get_reg_not_delivered_count(self)

    def as_amplifier(self):
        round = AmplifierRound.objects.get_current_round(self)
        return dict(
            id=self.id,
            first_name=self.first_name,
            last_name=self.last_name,
            avatar=self.avatar_url,
            email=self.email,
            round=round.as_json() if round is not None else '',
            city=self.city,
            info_contacts=self.info_contacts_count(),
            reg_contact=self.reg_contacts_count(),
            repeat_contact=self.repeat_contact(),
            current_info_contacts=self.current_info_contacts(),
            current_reg_contacts=self.current_reg_contacts(),
            current_repeat_contacts=self.current_repeat_contact(),
            reg_contact_open_count=self.opened_contacts_count(),
            reg_contact_not_open_count=self.not_opened_contacts_count(),
            reg_contact_not_delivered=self.not_delivered_contacts_count()
        )

    class Meta:
        verbose_name = u'Пользователь'
        verbose_name_plural = u'Пользователи'


