# -*- coding: utf-8 -*-
import base64
from django.conf import settings
from django.contrib.auth import authenticate, get_user_model
from django.core.exceptions import ValidationError
from django import forms
from django.core.mail import send_mail
from django.forms import ModelForm, PasswordInput
from django.template.defaultfilters import capfirst
from users.models import User
from django.utils.translation import ugettext as _

__author__ = 'andreyzakharov'


class RegistrationForm(forms.ModelForm):
    email = forms.EmailField(label=_('E-mail'),
                             widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'E-mail'}))
    name = forms.CharField(label=u'Имя', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Имя'}))
    last_name = forms.CharField(label=u'Фамилия',
                                widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Фамилия'}))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Пароль'}),
                                label=u'Пароль')
    password2 = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Пароль еще раз'}),
        label=u'Еще раз пароль')

    class Meta:
        model = User
        fields = ('email', 'password1', 'password2', 'name', 'last_name')

    def clean(self):
        email = self.cleaned_data.get('email')
        if email:
            users = User.objects.filter(email__iexact=email)
            if users:
                raise ValidationError(u"Этот  e-mail уже используется")
        else:
            raise ValidationError(u"E-mail не корректен")
        if self.cleaned_data.get('password1') and self.cleaned_data.get('password1') != self.cleaned_data.get(
                'password2'):
            raise ValidationError(u"Пароли не совпадают")

        return self.cleaned_data

    def save(self):
        super(RegistrationForm, self).save(commit=False)
        data = self.cleaned_data
        user = User.objects.create_user(data['email'], data['name'], data['name'], data['last_name'], data['password1'])
        return user


class AuthForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'E-mail'}),
                             label=u'Email')
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Пароль'}),
                               label=u'Пароль')

    class Meta:
        model = User
        fields = ('email', 'password')

    def __init__(self, request=None, *args, **kwargs):
        """
        If request is passed in, the form will validate that cookies are
        enabled. Note that the request (a HttpRequest object) must have set a
        cookie with the key TEST_COOKIE_NAME and value TEST_COOKIE_VALUE before
        running this validation.
        """
        self.request = request
        self.user_cache = None
        self.error_messages = []
        super(AuthForm, self).__init__(*args, **kwargs)

        # Set the label for the "username" field.
        UserModel = get_user_model()
        self.username_field = UserModel._meta.get_field(UserModel.USERNAME_FIELD)
        if not self.fields['email'].label:
            self.fields['email'].label = capfirst(self.username_field.verbose_name)

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        if email:
            user = User.objects.filter(email__iexact=email)
            if not user:
                raise ValidationError(u'Пользователь не существует')
            else:
                self.user_cache = authenticate(email=email, password=password)
                if self.user_cache is None:
                    raise forms.ValidationError(u'e-mail или пароль введены не правильно')
                elif not self.user_cache.is_active:
                    raise forms.ValidationError(_("User is not active"))
                self.check_for_test_cookie()
        else:
            raise ValidationError(u'E-mail некорректен')
        if not self.cleaned_data.get('password'):
            raise ValidationError(u'Неверный пароль')
        return self.cleaned_data

    def get_user(self):
        return self.user_cache

    def check_for_test_cookie(self):
        if self.request and not self.request.session.test_cookie_worked():
            raise forms.ValidationError(self.error_messages['no_cookies'])


class RememberPasswordForm(forms.Form):
    email = forms.EmailField(label=_('Email'), required=True)

    def clean_email(self):
        email = self.cleaned_data.get('email')

        self.exist_user = User.objects.filter(email__iexact=email)
        if not self.exist_user:
            raise ValidationError(_("This e-mail doesn't have any associated account. Please, register!"))

        return self.cleaned_data

    def save(self, token_generator, request=None):
        """
        Generates a one-use only link for resetting password and sends to the user
        """
        for user in self.users_cache:
            params = {
                'uid': base64.standard_b64encode('%d,%s' % (user.id, user.email)),
                'user_id': user.id,
                'token': token_generator.make_token(user),
            }
            try:
                if not send_mail(
                        template=getattr(settings, 'PROFILE_EMAIL_RESTORE_PASSWORD', 'profile_restore_password'),
                        recipient_list=[user.email], params=params):
                    self._errors["email"] = self.error_class([u'Invalid sending email to %s' % user.email])
                    raise self._errors["email"]

            except Exception as e:
                # logger.exception(e)
                return False

        return True


# class UserForm(ModelForm):
#     password = forms.CharField(widget=PasswordInput())
#
#     class Meta:
#         model = User
