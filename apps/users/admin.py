from django.contrib import admin

# Register your models here.
from django.contrib.admin.forms import AdminPasswordChangeForm
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm
from django import forms
from users.models import User
from django.utils.translation import ugettext_lazy as _


class UserCreationFormExtended(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(UserCreationFormExtended, self).__init__(*args, **kwargs)
        self.fields['email'] = forms.EmailField(label=_("E-mail"), max_length=75)


class CustomUserAdmin(UserAdmin):
    exclude = ()
    add_form = UserCreationFormExtended
    list_display = (
        'username', 'email', 'first_name', 'last_name', 'is_staff', 'birthday', 'country', 'city', 'info', 'mycube_id',
        'user_type')
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'email', 'username', 'password1', 'password2', 'birthday', 'country', 'city', 'info', 'mycube_id',
                'user_type')
        }),
    )
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'),
         {'fields': (
             'first_name', 'last_name', 'email', 'birthday', 'country', 'city', 'info', 'mycube_id', 'user_type')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )


admin.site.register(User, CustomUserAdmin)



#
# # admin.site.register(User)
