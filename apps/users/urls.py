from django.conf.urls import url
from .views import RegistrationFormView, AuthFormView, LogoutView

__author__ = 'andreyzakharov'
from . import pipeline
from . import views

urlpatterns = [
    url(r'social/(?P<backend>[^/]+)$', pipeline.register_by_access_token, name='social_auth'),
    url(r'register/', RegistrationFormView.as_view(template_name='users/registration.html'), name='register'),
    url(r'auth/', AuthFormView.as_view(template_name='users/auth.html'), name='login_user'),
    url(r'logout', LogoutView.as_view(), name='logout_user'),
    url(r'remember_pass', views.reset_user_password, name='forgot_password'),
]
