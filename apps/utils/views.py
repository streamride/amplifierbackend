from django.shortcuts import render
import time


def to_timestamp(date):
    return int(time.mktime(date.timetuple()))
