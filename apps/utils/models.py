import logging
import os

from django.core.files.storage import default_storage
from django.db import models
from datetime import datetime

from django.utils.deconstruct import deconstructible

IMAGE_PATH = 'i'


class UCDateTimeField(models.DateTimeField):
    def pre_save(self, model_instance, add):
        value = getattr(model_instance, self.attname)
        if self.auto_now or (self.auto_now_add and add) or value is None:
            value = datetime.now()
            setattr(model_instance, self.attname, value)
            return value
        else:
            if not isinstance(value, datetime):
                # assume that the value is a timestamp if it is not a datetime
                value = datetime.fromtimestamp(int(value))
                # an exception might be better than an assumption
                setattr(model_instance, self.attname, value)
            return super(UCDateTimeField, self).pre_save(model_instance, add)


logger = logging.getLogger('django')


def get_last_filename(default_storage):
    def _f(path):
        name, ext = os.path.splitext(path)
        try:
            return int(name)
        except:
            return 0

    d = [(path, _f(path)) for path in default_storage.listdir(IMAGE_PATH)[1]]
    d.sort(key=lambda x: x[1], reverse=True)
    return d and d[0][1] or 0


@deconstructible
class UploadToSpecDir(object):
    ppath = "i/{0}/{1}{2}"

    def __init__(self, sub_path):
        self.sub_path = sub_path

    def __call__(self, instance, filename):
        name = '{:0>5}{}'.format(get_last_filename(default_storage) + 1,
                                 os.path.splitext(filename)[-1])
        join = os.path.join(self.sub_path, name)
        # logger.debug(join)
        return join


class Agreement(models.Model):
    text_agreement = models.TextField()

    def __str__(self):
        return self.text_agreement

    def as_json(self):
        return dict(
            text=self.text_agreement
        )
