# coding: utf-8
import logging
import os
import re
import requests as r
import urllib.request

from django.core.files import File

from celery import Celery

from contacts.models import Contact
from django.conf import settings

logger = logging.getLogger('django')
app = Celery('tasks', broker=settings.BROKER_URL)

VK_URL = 'https://api.vk.com/method/users.get?user_ids={}&fields=photo,photo_big'
FB_URL = 'https://www.facebook.com/{}/'
FB_PICTURE_URL = 'https://graph.facebook.com/v2.8/{}/picture'
INSTAGRAM_URL = 'https://instagram.com/{}/'


@app.task
def load_from_social(contact_id):
    contact = Contact.objects.get(uuid=contact_id)
    if contact.avatar_from_set:
        if contact.vk_id:
            loadVk(contact=contact)
        elif contact.instagram_id:
            load_instagram(contact=contact)
        elif contact.fb_id:
            loadFb(contact=contact)


def load_instagram(contact):
    if contact.instagram_id.startswith("http"):
        url = contact.instagram_id
    else:
        url = INSTAGRAM_URL.format(contact.instagram_id)
    raw_html = r.get(url).text
    index = raw_html.find('profile_pic_url_hd')
    next_ap = raw_html.find('"', index + 24)
    image_url = raw_html[index + 22:next_ap]
    logger.debug(image_url)
    result = urllib.request.urlopen(image_url)
    contact.avatar.save(name=image_url, content=result)
    contact.avatar_from_set = False
    contact.save()
    logger.debug(result)
    logger.debug(contact.avatar)


def loadVk(contact):
    if contact.vk_id.startswith("http"):
        url = VK_URL.format(contact.vk_id.rsplit('/', 1)[0])
    else:
        url = VK_URL.format(contact.vk_id)
    result = r.get(url)
    json = result.json()
    if 'response' in json:
        for obj in json.get('response'):
            if 'photo_big' in obj:
                big_photo_url = obj.get('photo_big')
                result = urllib.request.urlopen(big_photo_url)
                contact.avatar.save(name=big_photo_url, content=result)
                contact.avatar_from_set = False
                contact.save()


def loadFb(contact):
    if contact.fb_id.startswith("http"):
        url = contact.fb_id
    else:
        url = FB_URL.format(contact.fb_id)
    result = r.get(url)
    index = result.text.find('entity_id')
    logger.debug(int(index))
    entity_id = result.text[index:index + 28]
    entity_id = int(re.search(r'\d+', entity_id).group())
    image_url = FB_PICTURE_URL.format(entity_id)
    result = urllib.request.urlopen(image_url)
    contact.avatar.save(name=image_url + '.jpg', content=result)
    contact.avatar_from_set = False
    contact.save()
