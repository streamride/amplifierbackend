# coding: utf-8
from concurrent.futures import ProcessPoolExecutor
from datetime import datetime

import logging
import time

import asyncio

import requests
from django.db.models import Q
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth import login as auth_login
from django.contrib.auth import authenticate as django_auth
from users.models import User
from validate_email import validate_email
from events.models import EventPhoto, Event
from contacts.models import ContactHistory, ContactLog, Contact, SmokeBrand, ContactPhoto, ContactGeo

from contacts.models import FavoriteContact
from .myqube_api import mycube_add_contact, mycube_update_contact, check_mq_email_duplicate
from .social import load_from_social

from utils.models import Agreement

logger = logging.getLogger('django')


@api_view(['POST', 'PUT', 'GET'])
@permission_classes([IsAuthenticated, ])
@csrf_exempt
def contacts(request):
    if request.method == 'POST':
        logger.debug(request.POST)
        logger.debug(request.FILES)
        contact = Contact.create(request.POST, request.user)
        contact.amplifier = request.user
        contact.save()
        if 'lng' in request.POST and 'lat' in request.POST:
            ContactGeo.objects.create(contact=contact, longitude=request.POST['lng'], latitude=request.POST['lat'])
        if 'avatar' in request.FILES:
            contact.avatar = request.FILES['avatar']
        contact.save()
        if contact.is_email_exists():
            if is_email_duplicate(contact.email):
                ContactHistory.set_status(contact=contact, status=ContactHistory.STATUS_REPEAT, user=request.user)
            else:
                ContactHistory.set_status(contact=contact, status=ContactHistory.STATUS_REG, user=request.user)
                ContactHistory.set_status(contact=contact, status=ContactHistory.STATUS_INFO, user=request.user)
        else:
            ContactHistory.set_status(contact=contact, status=ContactHistory.STATUS_INFO, user=request.user)

        check_mq_email_duplicate.delay(contact.uuid)
        load_from_social.delay(contact.uuid)
        mycube_add_contact.delay(contact.uuid)
        return JsonResponse(contact.as_json(), safe=False)
    elif request.method == 'PUT':
        contact = get_object_or_404(Contact, uuid=request.POST['id'])
        contact = Contact.update(contact, request.POST)

        if contact.is_email_exists():
            if is_email_duplicate(contact.email):
                ContactHistory.set_status(contact=contact, status=ContactHistory.STATUS_REG, user=request.user)
        if contact.is_repeat:
            ContactHistory.set_status(contact=contact, status=ContactHistory.STATUS_REPEAT, user=request.user)

        if 'lng' in request.POST and 'lat' in request.POST:
            ContactGeo.objects.create(contact=contact, longitude=request.POST['lng'], latitude=request.POST['lat'])
        if 'avatar' in request.FILES:
            contact.avatar = request.FILES['avatar']
        contact.save()
        if 'email' in request.POST:
            check_mq_email_duplicate.delay(contact.uuid)
        load_from_social.delay(contact.uuid)
        mycube_update_contact.delay(contact.uuid)
        return JsonResponse(contact.as_json(), safe=False)
    elif request.method == 'GET':
        if 'q' in request.GET:
            contacts = Contact.objects.filter(amplifier=request.user).filter(
                Q(name__contains=request.GET['q']) | Q(surname__contains=request.GET['q']))
        else:
            contacts = Contact.objects.get_by_user(user=request.user)
        result = [contact.as_json() for contact in contacts]
        return JsonResponse(result, safe=False)
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


def send_to_mycube(corouting):
    loop = asyncio.get_event_loop()
    try:
        if loop.is_closed():
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
        asyncio.ensure_future(corouting)
        loop.run_forever()
        # loop.run_until_complete(corouting)
    finally:
        loop.close()


@api_view(['GET'])
@permission_classes([IsAuthenticated, ])
@csrf_exempt
def current_contact(request, contact_uuid):
    if request.method == 'GET':
        contact = get_object_or_404(Contact, uuid=contact_uuid)
        return JsonResponse(contact.as_json())
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@api_view(['GET'])
@permission_classes([IsAuthenticated, ])
@csrf_exempt
def current_event(request, event_uuid):
    if request.method == 'GET':
        event = get_object_or_404(Event, uuid=event_uuid)
        return JsonResponse(event.as_json())
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@api_view(['POST', 'PUT', 'GET'])
@permission_classes([IsAuthenticated, ])
@csrf_exempt
def events(request):
    if request.method == 'POST':
        data = request.POST
        event = Event.create(data, request.user)
        event.save()
        logger.debug(request.FILES.getlist('files'))
        return JsonResponse(event.as_json())
    elif request.method == 'PUT':
        if 'id' not in request.POST:
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='id field is required')
        event = get_object_or_404(Event, uuid=request.POST.get('id'))
        event = Event.update(event, request.POST)
        event.save()
        return JsonResponse(event.as_json())
    elif request.method == 'GET':
        if 'q' in request.GET:
            events = Event.objects.filter(name__contains=request.GET['q'])
        else:
            events = Event.objects.filter(city=request.user.city)
        result = [event.as_json() for event in events]
        return JsonResponse(result, safe=False)
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@api_view(['POST', 'PUT', 'GET'])
@permission_classes([IsAuthenticated, ])
@csrf_exempt
def smoke_brands(request):
    if request.method == 'POST':
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)
    elif request.method == 'PUT':
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)
    elif request.method == 'GET':
        result = [smoke_brand.as_json() for smoke_brand in SmokeBrand.objects.all()]
        return JsonResponse(result, safe=False)
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@csrf_exempt
@api_view(['POST'])
def authenticate(request):
    if request.method == 'POST':
        data = request.POST
        user = User.objects.filter(email__iexact=data['email'])
        if user:
            user = django_auth(email=data['email'].lower(), password=data['password'])
            if not user:
                return HttpResponse(status=status.HTTP_403_FORBIDDEN, content='Password is incorrect')
            token, created = Token.objects.get_or_create(user=user)
            return JsonResponse(user.current(token.key))
        else:
            return HttpResponse(status=status.HTTP_403_FORBIDDEN, content='User with that email does not exist')
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@csrf_exempt
@api_view(['POST'])
def change_contact_status(request):
    if request.method == 'POST':
        data = request.POST
        if 'id' not in data or 'sent_status' not in data:
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST, content='id and sent_status fields are required')
        Contact.create_or_update_status(data=data)
        return HttpResponse(status=status.HTTP_200_OK)
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@permission_classes([IsAuthenticated, ])
@csrf_exempt
@api_view(['POST'])
def check_email_exist(request):
    if request.method == 'POST':
        mail = request.POST.get('email', '')
        is_valid = validate_email(mail, check_mx=True, debug=True, verify=True, smtp_timeout=30)
        return JsonResponse({'exist': 1 if is_valid else 0})
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@permission_classes([IsAuthenticated, ])
@csrf_exempt
@api_view(['GET'])
def smoke_brands(request):
    if request.method == 'GET':
        if 'q' in request.GET:
            q = request.GET['q']
            smoke_brands = SmokeBrand.objects.filter(name__contains=q)
        else:
            smoke_brands = SmokeBrand.objects.all()
        result = [smoke_brand.as_json() for smoke_brand in smoke_brands]
        return JsonResponse(result, safe=False)
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@permission_classes([IsAuthenticated, ])
@csrf_exempt
@api_view(['GET'])
def amplifier(request):
    if request.method == 'GET':
        return JsonResponse(request.user.as_amplifier())
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@permission_classes([IsAuthenticated, ])
@csrf_exempt
@api_view(['GET'])
def agreement(request):
    if request.method == 'GET':
        agreement = Agreement.objects.all()[0]
        return JsonResponse(agreement.as_json())
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@api_view(['POST'])
@permission_classes([IsAuthenticated, ])
@csrf_exempt
def upload_image(request, type):
    if request.method == 'POST':
        photos = []
        if type not in ['contact', 'event']:
            return HttpResponse(status=status.HTTP_403_FORBIDDEN)
        for file in request.FILES.getlist('file'):
            if type == 'contact':
                photo = ContactPhoto()
            else:
                photo = EventPhoto()
            photo.photo = file
            photo.save()
            photos.append(photo)
        result = [photo.as_json() for photo in photos]
        return JsonResponse(result, safe=False)
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@api_view(['POST', 'GET', 'DELETE'])
@permission_classes([IsAuthenticated, ])
@csrf_exempt
def favorite_contact(request):
    if request.method == 'GET':
        contacts = FavoriteContact.objects.get_my_favorites(amplifier=request.user)
        result = [f.as_json() for f in contacts]
        return JsonResponse(result, safe=False)
    elif request.method == 'POST':
        data = request.POST
        if 'contact_id' in data:
            contact = get_object_or_404(Contact, uuid=data['contact_id'])
            f, created = FavoriteContact.objects.get_or_create(contact=contact, amplifier=request.user)
            return JsonResponse(f.as_json(), safe=False)
        else:
            return JsonResponse({'error': 'field contact_id is required'}, safe=False)
    elif request.method == 'DELETE':
        data = request.GET
        if 'contact_id' in data:
            contact = get_object_or_404(Contact, uuid=data['contact_id'])
            f = FavoriteContact.objects.filter(contact=contact).filter(amplifier=request.user).first()
            if f is not None:
                f.delete()
            return HttpResponse(status=status.HTTP_202_ACCEPTED)
        else:
            return JsonResponse({'error': 'field contact_id is required'}, safe=False)
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@api_view(['GET'])
@permission_classes([IsAuthenticated, ])
@csrf_exempt
def check_email_duplicate(request):
    if request.method == 'GET':
        result = False
        email_ = request.GET['email']
        try:
            contact = Contact.objects.get(email=email_)
            result = True
        except:
            contact = None
        if contact is None:
            result = check_email_mycube(email_)
        return JsonResponse({'exists': result}, safe=False)
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


def check_email_mycube(email):
    url = 'http://myqube.ru/AppApi/check/'
    data = {"email": email}
    r = requests.post(url, json=data)
    res = False
    if r.status_code == 200:
        result = r.json()
        if result:
            if 'exists' in result:
                res = result.get('exists')
    return res


def is_email_duplicate(email):
    try:
        contact = Contact.objects.get(email=email)
    except:
        contact = None
    return contact is not None
