# coding: utf-8
import xlwt
from django.http import HttpResponse

from contacts.models import Contact

from utils.views import to_timestamp

from users.models import User
from amplifier_control.models import AmplifierGroup

from contacts.models import ContactHistory


def generate_report(request):
    start_date = request.GET.get('start_date', None)
    end_date = request.GET.get('end_date', None)
    a_admin_id = request.GET.get('aid', None)
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=amp_report.xls'
    wb = xlwt.Workbook(encoding='utf-8')
    make_first_sheet(wb, start_date, end_date, aid=a_admin_id)
    make_second_sheet(wb, start_date, end_date, aid=a_admin_id)
    wb.save(response)
    return response


def make_first_sheet(wb, start_date=None, end_date=None, aid=None):
    ws = wb.add_sheet("Sheet1")
    row_num = 0
    columns = [
        (u"Amplifier", 8000),
        (u"Адресат считает спамом", 6000),
        (u"Доступен", 6000),
        (u"Недоступен", 6000),
        (u"Grand Total", 6000),
        (u"Info", 6000),
        (u"Reg", 6000),
        (u"Rep", 6000),
    ]
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.font.height = 240
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num][0], font_style)
        ws.col(col_num).width = columns[col_num][1]
    font_style = xlwt.XFStyle()
    font_style.alignment.wrap = 1
    font_style.font.height = 200
    amplifiers = AmplifierGroup.objects.get_my_amplifiers(aid=aid)
    for amplifier in amplifiers:
        row_num += 1
        row = [
            amplifier.get_full_name(),
            Contact.objects.count_by_status(status=2, amplifier=amplifier, start_date=start_date, end_date=end_date),
            Contact.objects.count_by_status(status=1, amplifier=amplifier, start_date=start_date, end_date=end_date),
            Contact.objects.count_by_status(status=0, amplifier=amplifier, start_date=start_date, end_date=end_date),
            Contact.objects.get_count(amplifier=amplifier, start_date=start_date, end_date=end_date),
            ContactHistory.objects.get_info_count(amplifier=amplifier, start_date=start_date, end_date=end_date),
            ContactHistory.objects.get_reg_count(amplifier=amplifier, start_date=start_date, end_date=end_date),
            ContactHistory.objects.get_repeat_count(amplifier=amplifier, start_date=start_date, end_date=end_date)
        ]
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)
    row_num += 1
    pattern = xlwt.Pattern()
    pattern.pattern = xlwt.Pattern.SOLID_PATTERN
    pattern.pattern_fore_colour = xlwt.Style.colour_map['gray_ega']
    font_style.pattern = pattern
    row = [
        "Grand Total",
        Contact.objects.count_by_status(status=2, start_date=start_date, end_date=end_date, aid=aid),
        Contact.objects.count_by_status(status=1, start_date=start_date, end_date=end_date, aid=aid),
        Contact.objects.count_by_status(status=0, start_date=start_date, end_date=end_date, aid=aid),
        Contact.objects.get_count(start_date=start_date, end_date=end_date, aid=aid),
        ContactHistory.objects.get_info_count(start_date=start_date, end_date=end_date, aid=aid),
        ContactHistory.objects.get_reg_count(start_date=start_date, end_date=end_date, aid=aid),
        ContactHistory.objects.get_repeat_count(start_date=start_date, end_date=end_date, aid=aid)
    ]
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, row[col_num], font_style)


def make_second_sheet(wb, start_date=None, end_date=None, aid=None):
    ws2 = wb.add_sheet("Sheet2")
    columns = [
        (u"Амплифаер/консультант", 8000),
        (u"Фамилия потребителя", 8000),
        (u"Имя потребителя", 8000),
        (u"К какому событию привязан", 8000),
        (u"Геопозици", 8000),
        (u"Аватар", 8000),
        (u"Дата создания контакта", 8000),
        (u"Email", 8000),
        (u"Существует", 2000),
        (u"Статус", 6000),
        (u"Доставлено", 4000),
        (u"Открыто", 4000),
        (u"Клики", 4000),
        (u"Отписались", 4000),
        (u"Пожаловались на спам", 6000),
        (u"Информационный", 4000),
        (u"Регистрационный", 4000),
        (u"Повторный", 4000),
        (u"Ссылка на фотоподтверждение", 8000)

    ]
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.font.height = 240
    row_num = 0
    for col_num in range(len(columns)):
        ws2.write(row_num, col_num, columns[col_num][0], font_style)
        # set column width
        ws2.col(col_num).width = columns[col_num][1]
    font_style = xlwt.XFStyle()
    font_style.alignment.wrap = 1
    font_style.font.height = 200
    for contact in Contact.objects.get_all(start_date=start_date, end_date=end_date, aid=aid):
        row_num += 1
        row = [
            contact.amplifier.get_full_name(),
            contact.surname,
            contact.name,
            contact.get_event_name(),
            contact.get_geo(),
            contact.get_avatar_url(),
            contact.created.strftime('%d.%m.%Y'),
            contact.email,
            "yes" if contact.email else "no",
            contact.get_status_sent_ru(),
            contact.is_delivered(),
            "yes" if contact.open_status == 1 else "no",
            "yes" if contact.click_status == 1 else "no",
            "yes" if contact.unsubscribe_status == 1 else "no",
            "yes" if contact.sent_status == 2 else "no",
            contact.get_info_date(),
            contact.get_reg_date(),
            contact.get_repeat_date(),
            contact.get_photo_confirm()
        ]
        for col_num in range(len(row)):
            ws2.write(row_num, col_num, row[col_num], font_style)
