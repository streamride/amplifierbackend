# -*- coding: utf-8 -*-
import logging

import asyncio
import requests

from contacts.models import Contact, ContactHistory

from celery import Celery
from django.conf import settings

from utils.views import to_timestamp

logger = logging.getLogger('django')

base_host_url = 'http://myqube.ru/AppApi/'
app = Celery('tasks', broker=settings.BROKER_URL)


@app.task
def mycube_add_contact(contact_uuid):
    add_to_mycube(contact_uuid)
    logger.debug('add_contact is finished')


def add_to_mycube(contact_uuid):
    url = '{}{}'.format(base_host_url, 'add/')
    logger.debug(url)
    contact = Contact.objects.get(uuid=contact_uuid)
    data = {"name": contact.name,
            "surname": contact.surname,
            "phone": contact.get_phone(),
            "email": contact.get_email(),
            "birthday": contact.get_birthday_timestamp(),
            "is_smoke": int(contact.is_smoke),
            "comment": contact.get_comment(),
            "brand1": contact.get_brand1_name(),
            "brand2": contact.get_brand2_name(),
            "event_name": contact.get_event_name(),
            "created_date": to_timestamp(contact.created),
            "updated_date": to_timestamp(contact.updated),
            "is_agreed": int(contact.is_agreed),
            "amplifier_id": contact.get_mycube(),
            "contact_type": contact.get_contact_type(),
            "vk": contact.get_vk(),
            "fb": contact.get_fb(),
            "instagram": contact.get_instagram(),
            "app_id": str(contact.uuid)
            }
    logger.debug(str(data).encode('utf-8'))
    r = requests.post(url, json=data)
    logger.debug(r.text)
    if r.status_code == 200:
        result = r.json()
        logger.debug(result)
        if result:
            if 'id' in result:
                id = result.get('id')
                contact.my_cube_contact_id = id
                contact.save()


@app.task
def mycube_update_contact(contact_uuid):
    url = '{}{}'.format(base_host_url, 'update/')
    logger.debug(url)
    contact = Contact.objects.get(uuid=contact_uuid)
    url = '{}{}'.format(base_host_url, 'update')
    data = {"id": contact.my_cube_contact_id, "name": contact.name,
            "surname": contact.surname,
            "phone": contact.get_phone(),
            "email": contact.get_email(),
            "birthday": contact.get_birthday_timestamp(),
            "is_smoke": int(contact.is_smoke),
            "comment": contact.get_comment(),
            "brand1": contact.get_brand1_name(),
            "brand2": contact.get_brand2_name(),
            "event_name": contact.get_event_name(),
            "created_date": to_timestamp(contact.created),
            "updated_date": to_timestamp(contact.updated),
            "is_agreed": int(contact.is_agreed),
            "amplifier_id": contact.get_mycube(),
            "contact_type": contact.get_contact_type(),
            "vk": contact.get_vk(),
            "fb": contact.get_fb(),
            "instagram": contact.get_instagram(),
            "app_id": str(contact.uuid)}
    logger.debug('update_contact is finished')
    logger.debug(data)
    r = requests.post(url, data)
    r.json()


def my_cube_check_email_status(contact):
    if contact.email:
        url = '{}{}'.format(base_host_url, 'status')
        logger.debug(url)
        logger.debug('check status')
        r = requests.post(url, json={
            'email': contact.email
        })

        if r.status_code == 200:
            result = r.json()
            logger.debug(result)
            if 'status' in result:
                status = result.get('status')
                if status == 'sent':
                    contact.sent_status = 0
                elif status == 'delivered':
                    contact.sent_status = 1
                elif status == 'read':
                    contact.sent_status = 1
                    contact.open_status = 1
                elif status == 'activated':
                    contact.sent_status = 1
                    contact.open_status = 1
                    contact.click_status = 1
                contact.save()
                # sent, delivered, read, activated


@app.task
def check_mq_email_duplicate(contact_uuid):
    contact = Contact.objects.get(uuid=contact_uuid)
    logger.debug("check myqub duplicate email " + str(contact.email))
    url = '{}{}'.format(base_host_url, 'check')
    data = {"email": contact.email}
    r = requests.post(url, json=data)
    res = False
    if r.status_code == 200:
        result = r.json()
        logger.debug(result)
        if result and 'exists' in result:
            res = result.get('exists')
            if res:
                ContactHistory.set_status(contact=contact, status=ContactHistory.STATUS_REPEAT, user=contact.amplifier)
                ContactHistory.remove_other_statuses(contact=contact)
    return res
