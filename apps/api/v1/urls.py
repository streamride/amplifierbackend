# coding: utf-8
from django.conf.urls import include, url

from api.v1 import views

from api.v1 import report

urlpatterns = [
    url(r'contacts/(?P<contact_uuid>.+)/', views.current_contact, name='current_contact'),
    url(r'contacts/', views.contacts, name='contacts'),
    url(r'events/(?P<event_uuid>.+)/', views.current_event, name='current_event'),
    url(r'events/', views.events, name='events'),
    url(r'auth/', views.authenticate, name='auth'),
    url(r'smokebrands/', views.smoke_brands, name='smoke_brands'),
    url(r'validate_email', views.check_email_exist, name='check_email'),
    url(r'amplifier/', views.amplifier, name='amplifier'),
    url(r'profile/', views.amplifier, name='profile'),
    url(r'agreement/', views.agreement, name='agreement'),
    url(r'contact_status/', views.change_contact_status, name='change_contact_status'),
    url(r'upload/(?P<type>.+)/', views.upload_image, name='upload_image'),
    url(r'get_report/', report.generate_report, name='make_report'),
    url(r'favorite/', views.favorite_contact, name='favorite_contact'),
    url(r'check_email_duplicate/', views.check_email_duplicate, name='check_email_duplicate'),
]
