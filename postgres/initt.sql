CREATE USER amplifieruser with password 'amplifierpass';
CREATE DATABASE amplifier;
GRANT ALL PRIVILEGES ON DATABASE amplifier TO amplifieruser;
